import React from 'react'

import './Spinner.css'

const Spinner = () => <span className="Spinner" />

export default Spinner
