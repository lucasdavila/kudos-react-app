import React from 'react'

import './Toggle.css'

const Toggle = ({onClick}) => (
  <label className="switch">
    <input type="checkbox" onClick={onClick} />
    <span className="slider round" />
  </label>
)

export default Toggle
