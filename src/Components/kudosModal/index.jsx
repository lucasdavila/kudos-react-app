import React from 'react'
import Button from 'Components/common/Button'

import styles from './KudosModal.module.css'

class KudosModal extends React.Component {
  constructor(props) {
    super(props)
    this.textRef = React.createRef();
  }

  sendKudos = () => {
    const {onClick} = this.props
    onClick(this.textRef.current.value)
  }

  render() {
    const {closeCallback} = this.props

    return (
      <div className={styles['modal-container']}>
        <div className={styles.content}>
          <div className={styles.close} onClick={closeCallback}>x</div>

          <textarea ref={this.textRef}></textarea>

          <div className={styles['button-container']}>
            <Button onClick={this.sendKudos} text="Send" />
          </div>
        </div>
      </div>
    )
  }
}

export default KudosModal