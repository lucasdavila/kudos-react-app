import React, { Component } from 'react'

import { hot } from 'react-hot-loader/root'

import Spinner from 'Components/common/spinner'
import Toggle from 'Components/common/toggle'
import People from 'Components/people'

import people from 'Data/people'

import styles from './App.module.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { people: people, isLoading: true, darkTheme: true }
  }

  componentDidMount() {
    this.timeoutId = setTimeout(() => this.setState({ isLoading: false }), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timeoutId)
  }

  renderLoading() {
    return (
      <div className={`${styles.app} ${styles['loading-app']}`} data-theme={this.theme()}>
        <Spinner />
      </div>
    )
  }

  theme() {
    const {darkTheme} = this.state
    return darkTheme ? 'dark' : 'light'
  }

  toggleTheme = () => {
    this.setState({ darkTheme: !this.state.darkTheme })
  }

  render() {
    const {isLoading, people} = this.state
    let content

    if (isLoading) { return this.renderLoading() }

    return (
      <div className={styles.app} data-theme={this.theme()}>
        <h1 className={styles.title}>HELLO CLEARY</h1>

        <People people={people} />

        <div className={styles.toggle}>
          <Toggle onClick={this.toggleTheme} />
        </div>
      </div>
    )
  }
}

export default hot(App)
