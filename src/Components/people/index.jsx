import React from 'react'
import Card from './Card'

import styles from './People.module.css'

const People = (props) => (
  <div className={styles.people}>
    {props.people.map((person) =>
      <Card key={person.id} person={person} />
    )}
  </div>
)

export default People
