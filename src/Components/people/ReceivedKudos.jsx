import React from 'react'

import styles from './ReceivedKudos.module.css'
import cardStyles from './Card.module.css'

const ReceivedKudos = ({person, text}) => {
  const { firstName, lastName, profilePhoto } = person

  return (
    <div className={`${cardStyles.card} ${styles.card}`}>
      <div className={styles.contact}>
        <strong className={styles.name}>{ firstName } { lastName } </strong>
        Got some Kudos!
      </div>

      <img className={styles.avatar} src={profilePhoto} alt="profile photo" />

      <div className={styles.kudos}>
        "{ text }"
      </div>
    </div>
  )
}

export default ReceivedKudos
