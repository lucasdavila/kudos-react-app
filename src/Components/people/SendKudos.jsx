import React from 'react'
import Button from 'Components/common/Button'
import KudosModal from 'Components/KudosModal'

import styles from './SendKudos.module.css'
import cardStyles from './Card.module.css'

class SendKudos extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isModalVisible: false }
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  kudosCallback = (text) => {
    const {kudosCallback} = this.props
    kudosCallback(text)
  }

  render() {
    const {person: {firstName, lastName, email, title, profilePhoto}} = this.props
    const {isModalVisible} = this.state

    return (
      <div className={`${cardStyles.card} ${styles.card}`}>
        {isModalVisible &&
          <KudosModal onClick={this.kudosCallback} closeCallback={this.toggleModal} />
        }

        <img className={styles.avatar} src={profilePhoto} alt="profile photo" />

        <div className={styles.contact}>
          <strong className={styles.name}>
            { firstName } { lastName }
          </strong>

          <span className={styles.email}>
            | { email }
          </span>
        </div>

        <div className={styles.title}>
          { title }
        </div>

        <div className={styles['send-kudos']}>
          <Button text="Send Kudos" onClick={this.toggleModal} />
        </div>
      </div>
    )
  }
}

export default SendKudos
