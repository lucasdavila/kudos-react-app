import React from 'react'
import ReceivedKudos from './ReceivedKudos'
import SendKudos from './SendKudos'

class Card extends React.Component {
  constructor(props) {
    super(props)
    this.state = { kudosText: null }
  }

  handleKudos = (text) => {
    this.setState({ kudosText: text })
  }

  render() {
    const {person} = this.props
    const {kudosText} = this.state;

    if (kudosText) {
      return <ReceivedKudos person={person} text={kudosText} />
    } else {
      return <SendKudos person={person} kudosCallback={this.handleKudos} />
    }
  }
}

export default Card